## 1.2.1 / 2020-02-20

  * This is a valid version
  * Boombot

## 1.1.1 / 2012-02-20

  * Initial implementation
  * Tokenize a changelog (#1)

## 1.0.0 / 2015-02-21

### Major Enhancemens

  * Added that big feature (#1425)

### Bug Fixes

  * Fixed that narsty bug with tokenization (@carla)

## 0.0.1 / 2015-02-20

  * Initial implementation
  * Tokenize a changelog (#1)
