import sys
import os
import argparse
import logging
from junit_xml import TestSuite, TestCase

SOURCE_FILE = ".pyspellinglog"
TARGET_FILE = "spelling_junit.xml"

def process_chunk(chunk):
    """Process a chunk of data
    in order to create a TestCase

    Args:
        chunk (array): A list of string

    Returns:
        [TestCase]: Failed testcase from chunk
    """
    logging.debug("Process_chunk, content %s", chunk)
    file_infos = chunk[1].split(" ")
    file_path = file_infos[1]

    tests = []
    # len(chunk) - 1, so there not the separator line
    # in it
    for i in range(3, len(chunk)-1):

        try:
            file_name = "{0}, path: {1}".format(chunk[i], file_infos[2])
            test = TestCase(file_name, file_path, stderr=chunk[i])
            test.add_failure_info("Spell failed on {0}".format(chunk[i]))
            tests.append(test)
        except IndexError as error:
            logging.error("Error at index %s: %s", i, error)

    return tests

def is_success(line):
    """Define if the log sent a success,
    so there is no need to create a report

    Args:
        line (string): A single line of text

    Returns:
        [bool]: True or false if succeed
    """
    line = line.strip()
    logging.info("Pyspelling did succeed %s", line == "Spelling check passed :)")
    return line == "Spelling check passed :)"

def process_file(content):
    """Takes file content
    and create test case depending of
    each chunk of log

    Args:
        content (string): File content

    Returns:
        [Array]: A list of test case
    """
    test_cases = []
    chunk = []
    for line in content:
        line = line.strip()
        if not line:
            for chunk_part in process_chunk(chunk):
                test_cases.append(chunk_part)

            chunk = []
        else:
            chunk.append(line)

    return test_cases

def retrieve_file_chunks(file):
    """Retrieve file content,
    and process it to retrieve TestCase array

    Args:
        file (string): Content of pyspell command

    Returns:
        [Array]: List of TestCase
    """
    logging.debug("read_file, path %s", file)

    if not os.path.exists(file):
        logging.error("File %s does not exist!", file)
        sys.exit(1)

    with open(file, "r") as file_handle:
        filecontent = file_handle.readlines()

        if is_success(filecontent[len(filecontent)-1]):
            sys.exit(0)

        return process_file(filecontent)

def str2bool(value):
    """Parsing string to boolean

    Args:
        value (string): Value to parse

    Raises:
        argparse.ArgumentTypeError: If boolean is not correct

    Returns:
        [boolean]: True or false
    """
    if isinstance(value, bool):
        return value
    elif value.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif value.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def main():
    """Main function of this script
    """
    parser = argparse.ArgumentParser(description="Parsing PySpelling to JUnit")

    parser.add_argument("--debug", "-d",
    dest="loglevel", action="store_const", const=logging.DEBUG,
    default=logging.INFO, help="Logging Level")

    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)

    source_file = retrieve_file_chunks(SOURCE_FILE)
    test_suites = TestSuite("spell_check", source_file)

    with open(TARGET_FILE, 'w') as target_file:
        TestSuite.to_file(target_file, [test_suites])

    if len(source_file) > 0:
        sys.exit(1)


if __name__ == "__main__":
    main()
